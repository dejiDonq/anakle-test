<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Welcome </title>
<link rel="stylesheet" href="assets/css/bootstrap.min.css" type="text/css"  />
<link rel="stylesheet" href="style.css" type="text/css" />
</head>
<body>

	<nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="home.php">Anakle</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li ><a href="home.php">Home</a></li>
            <li class="active"><a href="upload.php">Upload</a></li>
            <li><a href="view.php">View</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
			  <span class="glyphicon glyphicon-user"></span>&nbsp;Hi' &nbsp;<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="logout.php?logout"><span class="glyphicon glyphicon-log-out"></span>&nbsp;Sign Out</a></li>
              </ul>
            </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav> 

	<div id="wrapper">

	<div class="container">
	    <div class="row">

<?php
ob_start();
error_reporting(1);

session_start();


include_once 'dbconnect.php';

if( !isset($_SESSION['user']) ) {
		header("Location: index.php");
		exit;
	}

extract($_POST);

$target_dir = "test_upload/";

$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);


        $fileDesc = trim($_POST['fileDesc']);
		

		$fileDesc = strip_tags($fileDesc);
	

		$fileDesc = htmlspecialchars($fileDesc);
		
	//	$display = "true";
		
		//$display = $_POST['display'];
		

		
		
	


if(isset($_POST["submit"]))
{
$check = filesize($_FILES["fileToUpload"]["tmp_name"]);
$mediapath = ($_FILES["fileToUpload"]["tmp_name"]);

$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);



if (file_exists($target_file)) {
    echo "<div class='alert alert-danger'>Sorry, file already exists.</div>";
  
}
else{
    
if ($_FILES["fileToUpload"]["size"] > 6000000) {
    echo "<div class='alert alert-danger'>Sorry, your file is too large.</div>";

    
}else{



if( $imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" && $imageFileType != "mp4" && $imageFileType != "avi"
&& $imageFileType != "mov" && $imageFileType != "3gp" && $imageFileType != "mpeg")

{
    echo "<div class='alert alert-danger'>File Format Not Suppoted</div>";
} 

else
{

$video_path=$_FILES['fileToUpload']['name'];

if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"],$target_file)){

mysqli_query($conn,"insert into video(video_name,file_format,file_desc) values('$video_path','$imageFileType','$fileDesc')");




echo "<div class='alert alert-success'>File Successfully uploaded </div>";

}

else {
    echo "<div class='alert alert-danger'>Something went wrong, Try again </div>";
}

}

}

}

}

//display all uploaded files

if($display)

{

$query=mysqli_query($conn, "select * from video");

	while($all_video=mysqli_fetch_array($query, MYSQLI_ASSOC))

	{

?>

	 <div class="col-lg-4 card">
	

	 <video width="300" height="200" controls>
	<source src="test_upload/<?php echo $all_video['video_name']; ?>" >
	</video> 
	
	</div>
	
	<?php } } ?>
	
	</div>
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	
	
<form method="post" enctype="multipart/form-data">

<div class="table-responsive" style="padding-left:2%; margin-top:5%" >
<table border="1" class="table table-striped">

<tr>

<Td>Upload  Video or Image</td></tr>

<Tr><td><input type="file" name="fileToUpload" id="fileToUpload" required /></td></tr>
<Tr><td><input type="text" name="fileDesc" id="fileDesc" placeholder="Brief File Description" required /></td></tr>

<tr><td>

<input type="submit" value="Upload Video or Image" name="submit"/>

<!--<button name="display"  value="Display Files">Display Files</button>-->


</td></tr>

</table>
</div>

</form>	


</div>
    
    </div>

<script>
$('#fileToUpload').bind('change', function() {

  //this.files[0].size gets the size of your file.
  if ((this.files[0].size) > 5000000){
      alert("File too Large, Max is 5MB");
      location.reload();
  }

});
</script>

 
    
    <script src="assets/jquery-1.11.3-jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    
</body>
</html>
<?php ob_end_flush(); ?>